﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class EndingA : MonoBehaviour
{
    IEnumerator Main()
    {
        var textMesh = FindObjectOfType<SuperTextMesh>();

        while (textMesh.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        GameObject.Find("fade").GetComponent<SpriteRenderer>().DOFade(1f, 5f);
    }
}
