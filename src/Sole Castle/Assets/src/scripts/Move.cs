﻿using PowerTools;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Move : MonoBehaviour
{
    private IEnumerator _main;
    private IEnumerator _jumpRoutine;
    private IEnumerator _dashCooldownRoutine;
    private IEnumerator _damageRoutine;
    
    private const float speed = 2f;
    private const float gravity = 5f;

    public int Health = 3;
    public int Stamina = 2;

    private SpriteAnim SpriteAnim;


    public AnimationClip DeathAnim;
    public AnimationClip StartAnim;
    public AnimationClip GetUpAnim;
    public AnimationClip WalkAnim;
    public AnimationClip IdleAnim;
    public AnimationClip AttackAnim;
    public AnimationClip Attack2Anim;
    public AnimationClip RecoverAnim;
    
    private AnimationClip KnightGetUpAnim;
    private AnimationClip KnightWalkAnim;
    private AnimationClip KnightIdleAnim;
    private AnimationClip KnightAttackAnim;
    private AnimationClip KnightAttack2Anim;
    private AnimationClip KnightRecoverAnim;

    public AnimationClip WoodStartAnim;
    public AnimationClip WoodGetUpAnim;
    public AnimationClip WoodWalkAnim;
    public AnimationClip WoodIdleAnim;
    public AnimationClip WoodAttackAnim;
    public AnimationClip WoodAttack2Anim;
    public AnimationClip WoodRecoverAnim;

    public AudioClip DeadClip;
    public AudioClip DashClip;
    public AudioClip SwingClip;
    public AudioClip HurtClip;
    public AudioClip LandClip;
    public AudioClip StepClip;

    private AudioSource SfxSource;


    GameManager gameManager;
    public void Start()
    {
        SfxSource = gameObject.AddComponent<AudioSource>();

        KnightGetUpAnim   = GetUpAnim;
        KnightWalkAnim    = WalkAnim;
        KnightIdleAnim    = IdleAnim;
        KnightAttackAnim  = AttackAnim;
        KnightAttack2Anim = Attack2Anim;
        KnightRecoverAnim = RecoverAnim;

        gameManager = FindObjectOfType<GameManager>();
        if (gameManager.NoLimitOnDash() && !gameManager.SeenBossCutscene)
        {
            Stamina = 30;

            StartAnim   = WoodStartAnim;
            GetUpAnim   = WoodGetUpAnim;
            WalkAnim    = WoodWalkAnim;
            IdleAnim    = WoodIdleAnim;
            AttackAnim  = WoodAttackAnim;
            Attack2Anim = WoodAttack2Anim;
            RecoverAnim = WoodRecoverAnim;
        }

        if (gameManager.InfiniteStamina())
        {
            Stamina = 99;
        }
        
        FindObjectOfType<PlayerGui>().Init(Health, Stamina);
    }



    float groundVelocity = 0f;
    public void Update()
    {
        if (gameManager.InfiniteStamina())
        {
            var rigidbody = GetComponent<Rigidbody2D>().velocity;
            var collider = transform.Find("collider").GetComponent<BoxCollider2D>();

            var yEdgeDistance = collider.size.y / 2f;
            var downWithDistance = Vector2.down * (yEdgeDistance);
            var yHits = Physics2D.BoxCastAll(transform.position, collider.size, 0f, downWithDistance, yEdgeDistance).Where(h => h.transform.name == "coat-collider");

            if (yHits.Count() > 0)
            {
                groundVelocity = 1f;
            }
            else
            {
                groundVelocity = 0f;
            }
        }
    }

    public void PlayStartAnim()
    {
        SpriteAnim = gameObject.GetComponentInChildren<SpriteAnim>();
        var spriteRenderer = SpriteAnim.GetComponent<SpriteRenderer>();
        spriteRenderer.color = Color.red;
        SpriteAnim.Play(StartAnim);
    }

    public void PlayGetUpAnim()
    {
        SpriteAnim = gameObject.GetComponentInChildren<SpriteAnim>();
        var spriteRenderer = SpriteAnim.GetComponent<SpriteRenderer>();
        spriteRenderer.color = Color.white;
        SpriteAnim.Play(GetUpAnim);
    }

    public void EnableControl()
    {
        _main = MainRoutine();
        StartCoroutine(_main);
    }

    IEnumerator moveRightRoutine;
    public void MoveRightUntilInterupt()
    {
        moveRightRoutine = MoveRight();
        StartCoroutine(moveRightRoutine);
    }

    public IEnumerator MoveRight()
    {
        SpriteAnim = gameObject.GetComponentInChildren<SpriteAnim>();

        if (!SpriteAnim.IsPlaying(WalkAnim))
        {
            SpriteAnim.Play(WalkAnim);
        }

        while (true)
        {
            GetComponent<Rigidbody2D>().velocity = (Vector2.right * speed) + (Vector2.down * gravity);
            yield return new WaitForEndOfFrame();
        }
    }

    public void OnStep()
    {
        SfxSource.PlayOneShot(StepClip, .5f);
    }

    public void StopMoving()
    {
        //TODO: Replace with idle animation
        SpriteAnim = gameObject.GetComponentInChildren<SpriteAnim>();
        SpriteAnim.Play(IdleAnim);
        GetComponent<Rigidbody2D>().velocity = (Vector2.down * gravity);
    }

    public IEnumerator AttackAndResetAnimations()
    {
        GetUpAnim = KnightGetUpAnim;
        WalkAnim = KnightWalkAnim;
        IdleAnim = KnightIdleAnim;
        AttackAnim = KnightAttackAnim;
        Attack2Anim = KnightAttack2Anim;
        RecoverAnim = KnightRecoverAnim;

        SpriteAnim.Play(AttackAnim);
        SfxSource.PlayOneShot(SwingClip);


        while (SpriteAnim.IsPlaying(AttackAnim))
        {
            yield return new WaitForEndOfFrame();
        }

        var attack1 = transform.Find("attack-1");
        var attackChildren = attack1.Cast<Transform>().ToList();
        foreach (var child in attackChildren)
        {
            child.gameObject.SetActive(false);
        }

        SpriteAnim.Play(IdleAnim);
    }

    private IEnumerator MainRoutine()
    {
        var gameManager = FindObjectOfType<GameManager>();

        Debug.Log("Enter main coroutine");
        SpriteAnim = gameObject.GetComponentInChildren<SpriteAnim>();

        // Just in case this gets missed somewhere else
        var spriteRenderer = SpriteAnim.GetComponent<SpriteRenderer>();
        spriteRenderer.color = Color.white;

        // SpriteAnim.Play(WalkAnim);
        SpriteAnim.Play(IdleAnim);

        // Safety check to make sure all the attack colliders are off
        var attack1 = transform.Find("attack-1");
        var attackChildren = attack1.Cast<Transform>().ToList();
        foreach (var child in attackChildren)
        {
            child.gameObject.SetActive(false);
        }
        
        while (true)
        {
            var jump = Input.GetAxisRaw("Jump");
            if (jump > 0 && Stamina > 0 && _jumpRoutine == null) // && _dashCooldownRoutine == null)
            {
                yield return Dash();
            }
            
            var attack = Input.GetAxisRaw("Fire1");
            if (attack > 0)
            {
                //Debug.Log("Got attack input... " + attack.ToString());
                yield return Attack();
                //Debug.Log("Done with attack");
            }
      
            var speedMod = 1f;
            var direction = Input.GetAxisRaw("Horizontal");
            if (direction < 0)
            {
                speedMod = .66f;
            }

            if (Mathf.Abs(direction) > 0f)
            {
                if (!SpriteAnim.IsPlaying(WalkAnim))
                {
                    SpriteAnim.Play(WalkAnim);
                }
            }
            else
            {
                if (!SpriteAnim.IsPlaying(IdleAnim))
                {
                    SpriteAnim.Play(IdleAnim);
                }
            }

            var rigidbody = GetComponent<Rigidbody2D>().velocity;
            var collider = transform.Find("collider").GetComponent<BoxCollider2D>();

            var xEdgeDistance = collider.size.x / 2f;

            var rightWithDistance = Vector2.right * (xEdgeDistance);
            var hits = Physics2D.BoxCastAll(transform.position, collider.size, 0f, rightWithDistance, xEdgeDistance).Where(h => h.transform.name == "Ground");

            var yVel = rigidbody.y;
            if (hits.Count() > 0 && Mathf.Abs(direction) > 0f)
            {
                yVel = 2f;
            }
            else if (yVel > 0f)
            {
                yVel = 0f;
            }
            
            GetComponent<Rigidbody2D>().velocity = new Vector2(((Vector2.right * speed * speedMod * direction) + (Vector2.down * gravity)).x + groundVelocity, yVel);
            
            yield return new WaitForEndOfFrame();
        }
    }

    public void Interupt(string type)
    {
        if (type == "attack")
        {
            var attack1 = transform.Find("attack-1");
            var attackChildren = attack1.Cast<Transform>().ToList();
            foreach (var child in attackChildren)
            {
                child.gameObject.SetActive(false);
            }
            
            if (_main != null)
            {
                StopCoroutine(_main);
                _main = null;
            }

            if (_damageRoutine == null)
            {
                _damageRoutine = TakeDamage(GetComponent<Rigidbody2D>());
                StartCoroutine(_damageRoutine);
            }
        }

        if (type == "goal")
        {
            var attack1 = transform.Find("attack-1");
            var attackChildren = attack1.Cast<Transform>().ToList();
            foreach (var child in attackChildren)
            {
                child.gameObject.SetActive(false);
            }

            if (_main != null)
            {
                StopCoroutine(_main);
                _main = null;
            }

            StartCoroutine(FinishTheLevel());
        }

        if (type == "boss-fight-start")
        {
            var attack1 = transform.Find("attack-1");
            var attackChildren = attack1.Cast<Transform>().ToList();
            foreach (var child in attackChildren)
            {
                child.gameObject.SetActive(false);
            }

            if (_main != null)
            {
                StopCoroutine(_main);
                _main = null;
            }
            
            StartCoroutine(FindObjectOfType<BossFightTrigger>().BossFightIntro());
        }

        if (type == "player-stop")
        {
            if (moveRightRoutine != null)
            {
                StopCoroutine(moveRightRoutine);
                moveRightRoutine = null;
            }

            StopMoving();
        }

        if (type == "gadget-punch")
        {
            var attack1 = transform.Find("attack-1");
            var attackChildren = attack1.Cast<Transform>().ToList();
            foreach (var child in attackChildren)
            {
                child.gameObject.SetActive(false);
            }

            if (_main != null)
            {
                StopCoroutine(_main);
                _main = null;
            }

            if (_damageRoutine == null)
            {
                transform.position = new Vector2(GameObject.Find("gadget-punch-end").transform.position.x, transform.position.y);
                _damageRoutine = TakeDamage(GetComponent<Rigidbody2D>(), new Vector2(-20f, 0f));
                StartCoroutine(_damageRoutine);
            }
        }
    }


    bool onGround = false;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
            
        if (collision.gameObject.layer == LayerMask.NameToLayer("Geometry"))
        {
            onGround = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Geometry"))
        {
            onGround = false;
        }
    }

    private IEnumerator TakeDamage(Rigidbody2D rigidBody, Vector3? velocityOverride = null)
    {
        var spriteRenderer = SpriteAnim.GetComponent<SpriteRenderer>();

        Health -= 1;

        FindObjectOfType<PlayerGui>().UpdateHealth(Health);

        transform.Find("feet").gameObject.SetActive(false);

        if (velocityOverride != null)
        {
            rigidBody.velocity = velocityOverride.Value;
        }

        if (Health <= 0)
        {
            if (velocityOverride == null)
            {
                rigidBody.velocity = Vector3.zero;
            }
            
            spriteRenderer.color = Color.red;

            // Stop collisions
            transform.Find("collider").gameObject.layer = LayerMask.NameToLayer("Effects");
            transform.Find("feet").gameObject.layer = LayerMask.NameToLayer("Effects");
            
            yield return BecomeDeath();

            yield break;
        }
        
        SpriteAnim = gameObject.GetComponentInChildren<SpriteAnim>();
        // SpriteAnim.Play(WalkAnim);

        SfxSource.PlayOneShot(HurtClip);

        if (velocityOverride == null)
        {
            rigidBody.velocity = (Vector3.up * 1f + Vector3.right * 1f);
        }
        
        rigidBody.position = new Vector2(rigidBody.position.x + .25f, rigidBody.position.y + .25f);

        
        spriteRenderer.color = Color.red;
        yield return new WaitForSeconds(.25f / 2f);
        spriteRenderer.color = Color.white;

        //SpriteAnim.Play(RecoverAnim);
        //rigidBody.freezeRotation = false;
        //rigidBody.angularVelocity = -60f;

        // yield return new WaitForSeconds(.25f);

        //rigidBody.freezeRotation = true;
        //rigidBody.angularVelocity = 0f;


        //rigidBody.rotation = 0f;
        //rigidBody.velocity = Vector3.zero;

        yield return new WaitForSeconds(.15f);

        transform.Find("feet").gameObject.SetActive(true);

        _main = MainRoutine();
        StartCoroutine(_main);

        _damageRoutine = null;
    }
    
    private IEnumerator TakeDamageWithCartoonySpin(Rigidbody2D rigidBody)
    {
        rigidBody.velocity = (Vector3.up * 6f + Vector3.left * 6f);
        rigidBody.position = new Vector2(rigidBody.position.x - .25f, rigidBody.position.y + .25f);
        rigidBody.freezeRotation = false;
        rigidBody.angularVelocity = -1200f;

        yield return new WaitForSeconds(4f);

        yield return FindObjectOfType<GameManager>().ResetLevel();
    }

    private IEnumerator BecomeDeath()
    {
        if (FindObjectOfType<GameManager>().CartoonySpinEnabled())
        {
            yield return TakeDamageWithCartoonySpin(GetComponent<Rigidbody2D>());
            yield break;
        }
        
        SpriteAnim = gameObject.GetComponentInChildren<SpriteAnim>();
        SpriteAnim.Play(DeathAnim);

        SfxSource.PlayOneShot(DeadClip);

        yield return new WaitForSeconds(4f);

        yield return FindObjectOfType<GameManager>().ResetLevel();
    }

    private IEnumerator FinishTheLevel()
    {
        FindObjectOfType<CameraFollow>().Pause = true;

        var moveRight = MoveRight();
        StartCoroutine(moveRight);
        
        yield return new WaitForSeconds(4f);
        
        StopCoroutine(moveRight);

        yield return FindObjectOfType<GameManager>().NextLevel();

        yield return null;
    }

    private IEnumerator Dash()
    {
        var yDirection = Input.GetAxisRaw("Vertical");
        var xDirection = Input.GetAxisRaw("Horizontal");
        if (xDirection == 0f && yDirection == 0f)
        {
            yield break;
        }

        SpriteAnim.Play(IdleAnim);

        SfxSource.PlayOneShot(DashClip, .75f);

        Stamina -= 1;

        FindObjectOfType<PlayerGui>().UpdateStamina(Stamina);

        var rigidBody = GetComponent<Rigidbody2D>();
        
        var speedMod = .66f;
     
        if (xDirection < 0)
        {
            speedMod = .66f;
        }

        var xVel = (xDirection * speedMod) * 8f * 2f;
        var yVel = (yDirection) * 12f;

        //var direction = new Vector2(xDirection / 3f, yDirection).normalized;

        var timer = .125f;
        while (timer > 0f)
        {
            rigidBody.velocity = new Vector2(xVel + groundVelocity, yVel);
            
            yield return new WaitForEndOfFrame();
            timer -= Time.deltaTime;
        }
        
        //yield return new WaitForSeconds(.125f);

        _dashCooldownRoutine = DashCooldown();
        StartCoroutine(_dashCooldownRoutine);

        rigidBody.velocity = new Vector2(groundVelocity, 0f);

        yield return new WaitForSeconds(.125f);

        _jumpRoutine = null;
    }
    
    private IEnumerator DashCooldown()
    {
        yield return new WaitForSeconds(2f);

        Stamina += 1;
        FindObjectOfType<PlayerGui>().UpdateStamina(Stamina);

        _dashCooldownRoutine = null;
    }

    //private IEnumerator Jump()
    //{
    //    onGround = false;

    //    var rigidBody = GetComponent<Rigidbody2D>();

    //    var yDirection = Input.GetAxis("Vertical");


    //    rigidBody.velocity = new Vector2(rigidBody.velocity.x, (Vector2.up * 6f).y);

    //    while (!onGround)
    //    {
    //        //// An air attack might be a little too strong for my tastes, might make more sense if you need to be on the ground
    //        //var attack = Input.GetAxis("Fire1");
    //        //if (attack > 0)
    //        //{
    //        //    Debug.Log("Got attack input... " + attack.ToString());
    //        //    yield return AirAttack();
    //        //    Debug.Log("Done with attack");
    //        //    break;
    //        //}

    //        var speedMod = 1f;
    //        var direction = Input.GetAxisRaw("Horizontal");
    //        if (direction < 0)
    //        {
    //            speedMod = .66f;
    //        }

    //        rigidBody.velocity += new Vector2(((Vector2.right * speed * speedMod * direction * Time.deltaTime)).x, (Vector2.down * 4f * Time.deltaTime).y);

    //        yield return new WaitForEndOfFrame();
    //    }

    //    _jumpRoutine = null;
    //}

    private IEnumerator Attack()
    {
        //GetComponent<Rigidbody2D>().velocity = Vector2.right * .5f;
        GetComponent<Rigidbody2D>().velocity = Vector2.right * 0f + new Vector2(groundVelocity, 0f);

        //Debug.Log("Doing Attack...");
        SpriteAnim.Play(AttackAnim);

        SfxSource.PlayOneShot(SwingClip);

        yield return new WaitForSeconds(AttackAnim.length);

        GetComponent<Rigidbody2D>().velocity = new Vector2(groundVelocity, 0f);

        //Debug.Log("Pow");

        yield return Recover(1);
    }

    private IEnumerator Attack2()
    {
        // GetComponent<Rigidbody2D>().velocity = Vector2.right * 1f;
        GetComponent<Rigidbody2D>().velocity = Vector2.right * 0f + new Vector2(groundVelocity, 0f);

        //Debug.Log("Doing Attack...");
        SpriteAnim.Play(Attack2Anim);

        SfxSource.PlayOneShot(SwingClip);

        yield return new WaitForSeconds(Attack2Anim.length);

        GetComponent<Rigidbody2D>().velocity = new Vector2(groundVelocity, 0f);

        //Debug.Log("Pow");

        yield return Recover(2);
    }

    public void AttackHitbox(int step)
    {
        var attack1 = transform.Find("attack-1");
        var attackChildren = attack1.Cast<Transform>().ToList();
        foreach (var child in attackChildren)
        {
            child.gameObject.SetActive(false);
        }

        attackChildren[step].gameObject.SetActive(true);
    }

    private IEnumerator Recover(int moveNo)
    {
        var attack1 = transform.Find("attack-1");
        var attackChildren = attack1.Cast<Transform>().ToList();
        foreach (var child in attackChildren)
        {
            child.gameObject.SetActive(false);
        }

        SpriteAnim.Play(RecoverAnim);
        
        Debug.Log("Recovering...");

        var time = .5f;
        while (time > 0f)
        {
            

            if (Input.GetAxisRaw("Fire1") > 0 && moveNo == 1)
            {
                yield return Attack2();
                yield break;
            }

            var jump = Input.GetAxisRaw("Jump");
            if (jump > 0 && _jumpRoutine == null && Stamina > 0)
            {
                yield return Dash();
                yield break;
            }
            
            //if (direction < 0 && moveNo == 1)
            //{
            //    GetComponent<Rigidbody2D>().velocity = (Vector2.left * 4f + Vector2.up * 2f );
            //    yield return new WaitForSeconds(.5f);
            //    yield break;
            //}
            //else if (direction > 0 && moveNo == 1)
            //{
            //    SpriteAnim.Play(WalkAnim);
            //    GetComponent<Rigidbody2D>().velocity = (Vector2.right * 4f + Vector2.up * 2f);
            //    yield return new WaitForSeconds(.5f);
            //    yield break;
            //}


            time -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        
        Debug.Log("All better");
    }

 }
