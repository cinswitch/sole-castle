﻿using UnityEngine;
using System.Collections;

public class EndingB : MonoBehaviour
{


    IEnumerator Main()
    {
        var songIsOver = GameObject.Find("song-is-over").GetComponent<SpriteRenderer>();

        var audioSource = GameObject.Find("song").GetComponent<AudioSource>();
        yield return new WaitForSeconds(audioSource.clip.length);

        songIsOver.enabled = true;
    }
}
