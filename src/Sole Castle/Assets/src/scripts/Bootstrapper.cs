﻿using UnityEngine;
using System.Collections;

public class Bootstrapper : MonoBehaviour
{
    public GameManager GameManagerPrefab;
    private static GameManager _gameManager;

    void Start()
    {
        if (_gameManager == null)
        {
            _gameManager = Instantiate(GameManagerPrefab);
        }
    }
}
