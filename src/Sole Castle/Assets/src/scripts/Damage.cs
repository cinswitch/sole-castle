﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var parent = transform.parent;

        // Welcome to Paul is a lazy idiot zone, please enjoy your stay
        if (collision.name == "goal")
        {    
            parent.GetComponent<Move>().Interupt("goal");
            return;
        }

        if (collision.name == "boss-fight-start")
        {
            parent.GetComponent<Move>().Interupt("boss-fight-start");
            return;
        }

        if (collision.name == "player-stop")
        {
            parent.GetComponent<Move>().Interupt("player-stop");
            return;
        }

        if (collision.name == "gadget-punch-collider")
        {
            parent.GetComponent<Move>().Interupt("gadget-punch");
            return;
        }

        parent.GetComponent<Move>().Interupt("attack");
    }
}
