﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;
using PowerTools;

public class Gadget : MonoBehaviour
{
    IEnumerator _attackPattern;

    IEnumerator _attackRecoil;

    private int Healths = 10;

    SpriteAnim SpriteAnim;
    public AnimationClip WalkAnimation;


    public AudioClip DeadClip;
    public AudioClip Hurt1Clip;
    public AudioClip Hurt2Clip;

    public GameObject Explosion;

    private AudioSource AudioSource;

    public void Start()
    {
        AudioSource = gameObject.AddComponent<AudioSource>();
        SpriteAnim = GetComponent<SpriteAnim>();
    }

    public void Interupt(string name)
    {
        if (name == "attack")
        {
            if (_attackRecoil == null)
            {
                _attackRecoil = ApplyAttack();
                StartCoroutine(_attackRecoil);
            }
        }
    }

    public void StartAttackPatterns()
    {
        if (_attackPattern == null)
        {
            _attackPattern = DoAttackPatterns();
            StartCoroutine(_attackPattern);
        }
    }

    private IEnumerator DoAttackPatterns()
    {
        SpriteAnim.Play(WalkAnimation);

        FindObjectOfType<GadgetArm>().Show();

        while (true)
        {
            FindObjectOfType<GadgetArm>().Reposition();

            FindObjectOfType<GadgetArm>().Fire();

            yield return new WaitForSeconds(2f);
        }
    }
    
    IEnumerator ApplyAttack()
    {
        Healths -= 1;
        if (Healths <= 0)
        {
            FindObjectOfType<GadgetPunch>().StopPunch();
            FindObjectOfType<GadgetArm>().Hide();

            yield return Die();
            yield break;
        }
        
        var sprite = GetComponent<SpriteRenderer>();

        var clipToPlay = Random.Range(0, 2);
        if (clipToPlay == 0)
        {
            AudioSource.PlayOneShot(Hurt1Clip);
        }
        else
        {
            AudioSource.PlayOneShot(Hurt2Clip);
        }

        sprite.color = Color.red;
        transform.DOShakePosition(.25f, .2f, 40);

        yield return new WaitForSeconds(.25f);

        sprite.color = Color.white;

        FindObjectOfType<GadgetPunch>().Punch();

        _attackRecoil = null;
    }

    IEnumerator Die()
    {
        AudioSource.PlayOneShot(DeadClip);

        StopCoroutine(_attackPattern);
        _attackPattern = null;

        FindObjectOfType<CameraFollow>().transform.parent = null;

        DOTween.Kill(transform.parent);
        transform.parent.DOMoveY(-10f, 5f).SetEase(Ease.Linear);
        transform.DOShakePosition(5f, 1, 90);

        var timer = 4f;
        while (timer > 0f)
        {
            timer -= Time.deltaTime;

            var exp = Instantiate(Explosion);
            exp.transform.position = (Vector2)transform.position + new Vector2(Random.Range(-2.5f, 2.5f), Random.Range(0f, 5f));
            
            yield return new WaitForSeconds(.1f);
            timer -= .1f;

            yield return new WaitForEndOfFrame();
        }

        //yield return new WaitForSeconds(4f);

        Camera.main.transform.Find("fade").GetComponent<SpriteRenderer>().DOColor(Color.white, 1f);

        yield return new WaitForSeconds(2f);

        SceneManager.LoadScene("TheEndChoice");
    }
}
