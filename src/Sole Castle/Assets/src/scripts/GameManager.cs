﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public GameObject PlayerPrefab;
    public GameObject SkeletonPrefab;
    public GameObject FleenstonesPrefab;
    public GameObject NozomiPrefab;
    public GameObject SkeletonThrowerPrefab;
    public GameObject GoalPrefab;

    public GameObject MusicPlayerPrefab;

    private int LevelNumber = 1;
    
    public bool SeenBossCutscene { get; set; } = false;

    public int CurrentLevel()
    {
        return LevelNumber;
    }

    public bool CartoonySpinEnabled()
    {
        return LevelNumber >= 3;
    }

    public bool NoLimitOnDash()
    {
        return LevelNumber >= 4;
    }

    public bool InfiniteStamina()
    {
        return LevelNumber == 5;
    }

    void Start ()
    {
        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += OnSceneLoaded;

        var currentScene = SceneManager.GetActiveScene();
        UpdateLevelNumber(currentScene);

        StartCoroutine(StartLevelTransition());
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        UpdateLevelNumber(scene);

        // If it's a level that contains a player start then it's a regular level, right?
        var playerStart = GameObject.Find("player-start");
        if (playerStart != null)
        {
            StartCoroutine(StartLevelTransition());
            return;
        }

        if (scene.name == "Intermission")
        {
            StartCoroutine(FindObjectOfType<Intermission>().WaitForEndOfText(LevelNumber));

            //LevelNumber += 1;
        }
    }
    
    private void UpdateLevelNumber(Scene scene)
    {
        if (scene.name.StartsWith("Area-"))
        {
            var sceneNumber = scene.name.Replace("Area-", "");


            int parsedSceneNumber;
            if (int.TryParse(sceneNumber, out parsedSceneNumber))
            {
                LevelNumber = parsedSceneNumber;
            }
        }
    }

    public IEnumerator StartLevelTransition()
    {
        Debug.Log("StartLevelTransition");

        var musicPlayerGameObject = Instantiate(MusicPlayerPrefab);
        musicPlayerGameObject.name = MusicPlayerPrefab.name;
        var musicPlayer = musicPlayerGameObject.GetComponent<MusicPlayer>();

        // get the starting point for the level
        // We should create a "player start" tile, if possible.
        var playerStart = GameObject.Find("player-start");
        
        var spawnTiles = GameObject.Find("Spawns");
        
        // find or create player character
        var player = GameObject.Find("person");
        if (player == null)
        {
            player = Instantiate(PlayerPrefab, playerStart.transform.position, Quaternion.identity);
            player.name = PlayerPrefab.name;
        }

        var skeletonSpawnList = spawnTiles.transform.Cast<Transform>().ToList();
        foreach (var s in skeletonSpawnList)
        {
            if (s.name == "player-start")
            {
                player.transform.position = playerStart.transform.position;
            }
            if (s.name == "skeleton-spawn")
            {
                var prefab = SkeletonPrefab;
                if (CartoonySpinEnabled())
                {
                    prefab = FleenstonesPrefab;
                }

                var skele = Instantiate(prefab, s.transform.position, Quaternion.identity);
                skele.name = prefab.name;
            }
            if (s.name == "skeleton-thrower-spawn")
            {
                var prefab = SkeletonThrowerPrefab;
                if (CartoonySpinEnabled())
                {
                    prefab = NozomiPrefab;
                }

                var skele = Instantiate(prefab, s.transform.position, Quaternion.identity);
                skele.name = prefab.name;
            }
            if (s.name == "goal-spawn")
            {
                var goal = Instantiate(GoalPrefab, s.transform.position, Quaternion.identity);
                goal.name = GoalPrefab.name;
            }
        }
        
        spawnTiles.SetActive(false);

        // tell the player they can't do any moving yet
        var playerScript = player.GetComponent<Move>();

        playerScript.PlayStartAnim();

        yield return new WaitForSeconds(2f);

        playerScript.PlayGetUpAnim();

        //player.transform.Find("sprite").transform.DOShakePosition(1.5f, .25f, 40);
        //TODO: Play animation of the player armor assembling
        yield return new WaitForSeconds(.5f);
        
        playerScript.EnableControl();

        if (SceneManager.GetActiveScene().name == "Intro")
        {
            // no op
        }
        else if (LevelNumber == 4)
        {
            musicPlayer.PlaySong(MusicPlayer.Song.SkeletonKiller3);
        }
        else if (LevelNumber == 3)
        {
            musicPlayer.PlaySong(MusicPlayer.Song.SkeletonKiller2);
        }
        else if (LevelNumber == 1 || LevelNumber == 2)
        {
            musicPlayer.PlaySong(MusicPlayer.Song.SkeletonKiller1);
        }

        yield return null;
    }

    public IEnumerator NextLevel()
    {
        Camera.main.transform.Find("fade").GetComponent<SpriteRenderer>().DOColor(Color.black, 1f);

        FindObjectOfType<MusicPlayer>().FadeOut();

        yield return new WaitForSeconds(3f);

        var activeScene = SceneManager.GetActiveScene();
        if (activeScene.name == "Intro")
        {
            LevelNumber = 1;
            SceneManager.LoadScene("Area-01");
        }
        else
        {
            SceneManager.LoadScene("Intermission");
        }

        yield return null;
    }

        public IEnumerator ResetLevel()
    {
        Camera.main.transform.Find("fade").GetComponent<SpriteRenderer>().DOColor(Color.black, 1f);

        yield return new WaitForSeconds(3f);

        SceneManager.LoadScene($"Area-{LevelNumber.ToString("D2")}");
        
        yield return null;
    }
}
