﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class IntroductionSequence : MonoBehaviour
{
    public GameObject FirstScreen;
    public GameObject SecondScreen;
    public GameObject ThirdScreen;

    public GameObject BottomText;
    public GameObject TopText;

    // First screen
    // Text bottom
    private string FirstLine = @"There once was a time when I was king of this peaceful kingdom.<br>The people flourished, the harvest plentiful... all was right in the realm.";

    // Fade to second screen during this line
    // Text bottom
    private string SecondLine = @"But that all changed one ill-fated night... when the sorceress attacked.";

    // Second Screen
    // Text bottom
    private string ThirdLine = @"She placed a curse on the kingdom that turned one against another with murderous intent.<br><br>The streets ran red with the rivers of our blood.  The screams of the dead and dying reaching an unending crescendo.  By the first light of dawn, none remained alive.";

    // Second Screen
    // Text bottom
    private string FourthLine = @"But even in death we found no sanctuary from her dark arts.<br>She encapsulated our souls within this dark enclave, which she had<br>transformed into her dwelling place.  Here our despair and misery served<br>only to fuel her blighted power.";

    // ThirdScreen
    // Text top
    private string FifthLine = @"All hope was lost.";

    // ThirdScreen
    // Text top
    private string SixthLine = @"That is, until the eve of the hundredth year of our enslavement.<br>For whatever purpose, the sorceress’s thrall over me slipped.<br>And in that moment I knew what I had to do.<br><br>There was only one thing to be done,<br>and nothing would stand in my way.";

    // Fade out ThirdScreen
    // Text top, hold & give the player control
    private string SeventhLine = @"I took up my axe, and sought out the architect of our suffering.<br><br>The time of <c=red>Retribution</c> had come.";

    IEnumerator IntroRoutine;

    void Start()
    {
        IntroRoutine = PlaySequence();
        StartCoroutine(IntroRoutine);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S) && IntroRoutine != null)
        {
            StopCoroutine(IntroRoutine);
            BottomText.SetActive(false);
            TopText.SetActive(false);
            FirstScreen.SetActive(false);
            SecondScreen.SetActive(false);
            ThirdScreen.SetActive(false);

            IntroRoutine = null;
        }
    }

    // Update is called once per frame
    IEnumerator PlaySequence()
    {
        yield return PlayTextOnBottomAndWaitForItToEnd(FirstLine);

        FirstScreen.GetComponent<SpriteRenderer>().DOFade(0f, 1f);

        yield return PlayTextOnBottomAndWaitForItToEnd(SecondLine);

        yield return PlayTextOnBottomAndWaitForItToEnd(ThirdLine);

        yield return PlayTextOnBottomAndWaitForItToEnd(FourthLine);

        BottomText.SetActive(false);
        TopText.SetActive(true);

        SecondScreen.GetComponent<SpriteRenderer>().DOFade(0f, 1f);

        yield return PlayTextOnTopAndWaitForItToEnd(FifthLine);
        
        yield return PlayTextOnTopAndWaitForItToEnd(SixthLine);

        ThirdScreen.GetComponent<SpriteRenderer>().DOFade(0f, 1f);

        yield return PlayTextOnTopAndWaitForItToEnd(SeventhLine);

        yield return null;
    }

    IEnumerator PlayTextOnBottomAndWaitForItToEnd(string text)
    {
        var stm = BottomText.GetComponent<SuperTextMesh>();

        stm.Text = text;
        while (stm.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2f);

        yield return null;
    }

    IEnumerator PlayTextOnTopAndWaitForItToEnd(string text)
    {
        var stm = TopText.GetComponent<SuperTextMesh>();

        stm.Text = text;
        while (stm.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2f);

        yield return null;
    }
}
