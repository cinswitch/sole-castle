﻿using UnityEngine;
using System.Collections;
using PowerTools;

public class GadgetPunch : MonoBehaviour
{
    GameObject punchCollider;

    public AnimationClip PunchAnim;
    SpriteAnim SpriteAnim;

    public AudioClip PunchClip;
    public AudioClip PunchWindup;
    AudioSource AudioSource;

    void Start()
    {
        AudioSource = gameObject.AddComponent<AudioSource>();
        SpriteAnim = GetComponent<SpriteAnim>();

        punchCollider = transform.Find("gadget-punch-collider").gameObject;
        punchCollider.SetActive(false);
    }

    public void Punch()
    {
        if (!SpriteAnim.IsPlaying(PunchAnim))
        {
            SpriteAnim.Play(PunchAnim);
        }
    }

    public void StopPunch()
    {
        PunchOff();
        SpriteAnim.Stop();
    }

    public void PlayPunchWindup()
    {
        AudioSource.PlayOneShot(PunchWindup);
    }

    public void PunchOn()
    {
        AudioSource.PlayOneShot(PunchClip);
        punchCollider.SetActive(true);
    }

    public void PunchOff()
    {
        punchCollider.SetActive(false);
    }
}
