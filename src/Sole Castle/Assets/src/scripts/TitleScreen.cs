﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class TitleScreen : MonoBehaviour
{
    IEnumerator _startGame;
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) && _startGame == null)
        {
            _startGame = StartGame();
            StartCoroutine(_startGame);
        }

        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.Alpha5))
        {
            SceneManager.LoadScene("Area-05");
        }
    }

    public IEnumerator StartGame()
    {
        GameObject.Find("fade").GetComponent<SpriteRenderer>().DOColor(Color.black, 1f); ;

        yield return new WaitForSeconds(3f);

        SceneManager.LoadScene("Intro");
        yield return null;
    }

}
