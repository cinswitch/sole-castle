﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossFightTrigger : MonoBehaviour
{
    public GameObject GadgetLaser;

    public GameObject KnightText;
    public GameObject WoodText;
    public GameObject GadgetText;

    void Start ()
    {
        var gameManager = FindObjectOfType<GameManager>();
        if (gameManager.SeenBossCutscene)
        {
            var woodman = GameObject.Find("woodman");
            Destroy(woodman);
        }

        KnightText.SetActive(false);
        WoodText.SetActive(false);
        GadgetText.SetActive(false);
    }

    private IEnumerator TheBossFightCutscene()
    {
        var gameManager = FindObjectOfType<GameManager>();

        var cameraFollow = FindObjectOfType<CameraFollow>();
        cameraFollow.Player = GameObject.Find("boss-fight-center");

        var backCollider = GameObject.Find("back-collider");

        var woodman = GameObject.Find("woodman");
        var player = FindObjectOfType<Move>();

        // Woodman
        // Haha, what up, other Woodman?

        yield return ShowWoodText("Haha, what up, other Woodman?");


        // <j>ENOUGH OF THIS JACKNAPERY, FOUL DEMON!!</j>
        // I will not sit idly by and let my home be destroyed by some interloper!!!
        yield return ShowKnightText("<j>ENOUGH OF THIS JACKNAPERY, FOUL DEMON!!</j>");

        StartCoroutine(player.AttackAndResetAnimations());
        yield return new WaitForSeconds(.2f);
        woodman.transform.DOMove(new Vector2(woodman.transform.position.x + 1f, woodman.transform.position.y), .25f);

        yield return ShowKnightText("I will not sit idly by and let my home be destroyed by some interloper!!!");

        /*
         hey dude, could ya stop?
         i'm trying to give this shitty game a cool new look. <d=12>
         we're gonna make mad bitcoin
        */
        yield return ShowWoodText(@"hey dude, could ya stop?
i'm trying to give this shitty game a cool new look. <d=12>
we're gonna make mad bitcoin");

        // Where is the sorceress?
        yield return ShowKnightText("Where is the sorceress?");
        
        // i dunno, dead?
        yield return ShowWoodText("i dunno, dead?");

        // !!!
        yield return ShowKnightText("!!!");

        /*
         anyways, this is boring. i'm bored.
         i think its time for a leaf attack!
        */
        yield return ShowWoodText(@"anyways, this is boring. i'm bored.
i think its time for a leaf attack!");

        //var laser = Instantiate(GadgetLaser);
        //laser.transform.position = (Vector2)woodman.transform.position - (new Vector2(-5f, -2.5f) * 5f);
        //laser.layer = LayerMask.NameToLayer("PlayerAttacks");

        FindObjectOfType<GadgetArm>().Fire();

        yield return new WaitForSeconds(1.5f);

        // Laser shoots woodman, who violently spins off screen

        var woodbody = woodman.GetComponent<Rigidbody2D>();
        woodbody.WakeUp();
        woodbody.velocity = (Vector3.up * 24f + Vector3.left * 12f);
        woodbody.position = new Vector2(woodbody.position.x + .25f, woodbody.position.y + .25f);
        woodbody.freezeRotation = false;
        woodbody.angularVelocity = 2400f;

        Destroy(woodman, 5f);

        yield return new WaitForSeconds(2f);

        // <c=green><j>I  A M  B E T T E R  T H A N  Y O U  A R E  S O  I  W I L L  D O  T H E  R E V I E W</j></c>
        yield return ShowGadgetText(@"<c=green><j>I  A M  B E T T E R  T H A N  Y O U  A R E  S O  I  W I L L  D O  T H E  D E S I G N</j></c>");

        var targetGadgetPos = GameObject.Find("gadget-onscreen-pos").transform.localPosition;
        var gadget = GameObject.Find("Gadget");
        gadget.transform.DOLocalMoveX(targetGadgetPos.x, 10f).SetEase(Ease.Linear);

        FindObjectOfType<MusicPlayer>().PlaySong(MusicPlayer.Song.BossIntro);

        // This powerful energy... is he the true source of corruption?
        yield return ShowKnightText("This powerful energy... is he the true source of corruption?");
        
        // <c=green><j>B R O W N  B R I C K S</j></c>
        yield return ShowGadgetText(@"<c=green><j>W H A T  A N  A S S H O L E</j></c>");

        // Even if it means my destruction...
        yield return ShowKnightText("Even if it means my destruction...");
        // <j>YOU WILL MEET YOUR END HERE!</j>
        yield return ShowKnightText("<j>YOU WILL MEET YOUR END HERE!</j>");
    }

    private IEnumerator ShowKnightText(string text)
    {
        KnightText.SetActive(true);
        var stm = KnightText.GetComponent<SuperTextMesh>();
        stm.Text = text;

        while (stm.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        
        yield return new WaitForSeconds(2f);

        KnightText.SetActive(false);
    }

    private IEnumerator ShowWoodText(string text)
    {
        WoodText.SetActive(true);
        var stm = WoodText.GetComponent<SuperTextMesh>();
        stm.Text = text;

        while (stm.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(2f);

        WoodText.SetActive(false);
    }

    private IEnumerator ShowGadgetText(string text)
    {
        GadgetText.SetActive(true);

        var stm = GadgetText.GetComponent<SuperTextMesh>();
        stm.Text = text;

        yield return new WaitForSeconds(5f);

        GadgetText.SetActive(false);
    }


    public IEnumerator BossFightIntro()
    {
        var gameManager = FindObjectOfType<GameManager>();
        
        var cameraFollow = FindObjectOfType<CameraFollow>();
        cameraFollow.Player = GameObject.Find("boss-fight-center");

        var backCollider = GameObject.Find("back-collider");

        var woodman = GameObject.Find("woodman");
        var player = FindObjectOfType<Move>();
        
        // Walk the player to woodman
        player.MoveRightUntilInterupt();
        
        yield return new WaitForSeconds(3f);
        
        cameraFollow.Pause = true;
        cameraFollow.transform.parent = transform;

        backCollider.GetComponent<BoxCollider2D>().enabled = true;
        
        if (!gameManager.SeenBossCutscene)
        {
            yield return new WaitForSeconds(2f);

            yield return TheBossFightCutscene();

            
            // Player attack animation, woodman dodges
            // Player has normal sprite set again
        }
        else
        {
            var targetGadgetPos = GameObject.Find("gadget-onscreen-pos").transform.localPosition;
            var gadget = GameObject.Find("Gadget");
            gadget.transform.DOLocalMoveX(targetGadgetPos.x, 2f).SetEase(Ease.InExpo);

            yield return new WaitForSeconds(2f);
        }
        
        if (!gameManager.SeenBossCutscene)
        {
            // is this the true source of the corruption?
        }
        else
        {
            FindObjectOfType<MusicPlayer>().PlaySong(MusicPlayer.Song.BossLoop);
        }
        // commence boss fight!

        player.EnableControl();

        gameManager.SeenBossCutscene = true;

        transform.DOMoveX(390f, 1f).SetEase(Ease.Linear).SetSpeedBased();

        FindObjectOfType<Gadget>().StartAttackPatterns();
    }
}
