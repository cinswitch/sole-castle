﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    public enum Song
    {
        SkeletonKiller1,
        SkeletonKiller2,
        SkeletonKiller3,
        BossIntro,
        BossLoop,
    }

    public AudioClip SkeletonKiller1;
    public AudioClip SkeletonKiller2;
    public AudioClip SkeletonKiller3;

    public AudioClip BossIntro;
    public AudioClip BossLoop;

    private AudioSource AudioSource;
    private AudioSource AudioSourceSecondary;

    public void PlaySong(Song song)
    {
        if (AudioSource == null)
        {
            AudioSource = gameObject.AddComponent<AudioSource>();
            AudioSource.loop = true;
            AudioSource.volume = .5f;
        }

        if (AudioSourceSecondary == null)
        {
            AudioSourceSecondary = gameObject.AddComponent<AudioSource>();
            AudioSourceSecondary.loop = true;
            AudioSourceSecondary.volume = .5f;
        }

        switch (song)
        {
            case Song.SkeletonKiller1:
                AudioSource.clip = SkeletonKiller1;
                AudioSource.loop = true;
                AudioSource.Play();
                break;
            case Song.SkeletonKiller2:
                AudioSource.clip = SkeletonKiller2;
                AudioSource.loop = true;
                AudioSource.Play();
                break;
            case Song.SkeletonKiller3:
                AudioSource.clip = SkeletonKiller3;
                AudioSource.loop = true;
                AudioSource.Play();
                break;
            case Song.BossIntro:
                AudioSource.clip = BossIntro;
                AudioSource.loop = false;

                AudioSourceSecondary.clip = BossLoop;
                AudioSourceSecondary.loop = true;

                StartCoroutine(PlaySecondaryAudioSourceAfterFirstFinishes());
                break;
            case Song.BossLoop:
                AudioSource.clip = BossLoop;
                AudioSource.loop = true;
                AudioSource.Play();
                break;
        }
    }

    private IEnumerator PlaySecondaryAudioSourceAfterFirstFinishes()
    {
        AudioSource.Play();

        while (AudioSource.isPlaying)
        {
            yield return new WaitForEndOfFrame();
        }

        AudioSourceSecondary.Play();

        yield return null;
    }

    public void FadeOut()
    {
        AudioSource.DOFade(0f, 2f);
    }
}
