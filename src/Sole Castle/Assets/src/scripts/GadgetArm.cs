﻿using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using PowerTools;
using System.Linq;

public class GadgetArm : MonoBehaviour
{

    List<GameObject> laserColliders;

    public AnimationClip FireAnim;
    public AnimationClip IdleAnim;
    SpriteAnim SpriteAnim;

    public AudioClip LaserClip;
    AudioSource AudioSource;

    void Start()
    {
        AudioSource = gameObject.AddComponent<AudioSource>();

        SpriteAnim = GetComponent<SpriteAnim>();

        laserColliders = transform.Cast<Transform>().Select(t => t.gameObject).ToList();
        laserColliders.ForEach(l => l.SetActive(false));
    }
    
    public void Show()
    {
        transform.DOLocalMoveX(3.83f, 1f);
    }

    public void Hide()
    {
        SpriteAnim.Play(IdleAnim);

        transform.DOLocalMoveX(7.5f, .25f);
    }

    public void Reposition()
    {
        transform.DORotate(new Vector3(0f, 0f, Random.Range(1f, 18f)), .15f);
        transform.DOMoveY(Random.Range(0f, 1.9f), .15f);    
    }

    public void LaserSound()
    {
        
    }

    public void Fire()
    {
        if (!SpriteAnim.IsPlaying(FireAnim))
        {
            SpriteAnim.Play(FireAnim);
        }
    }

    public void SetLaser(int index)
    {
        laserColliders.ForEach(l => l.SetActive(false));

        if (laserColliders.Count() - 1 < index)
        {
            return;
        }

        laserColliders[index].SetActive(true);

        if (index == 0)
        {
            AudioSource.PlayOneShot(LaserClip);
        }
    }
}
