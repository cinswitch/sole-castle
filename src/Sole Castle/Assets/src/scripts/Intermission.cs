﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using PowerTools;

public class Intermission : MonoBehaviour
{
    private string[] TheScript =
    {
        "As the Final Knight set out, <d=12>Countless Wretches were summoned<br><br>To reach the Sorceress, he would crush those he once called countrymen.",

        "Time after time, the Final Knight smashed those that stood in his way.<d=12><br><br>Very little of his humanity remained, armor driven by pure <c=red>hatred</c>.",

        "Final Knight could feel it,<br>the castle's heart and soul.  <d=12><br><br>Once it was destroyed...<br>something cool would probably happen. <d=4>N<d=4>i<d=4>c<d=4>e<d=4>. <d=4>><d=4>:<d=4>]",

        "<d=12>...<br>"
    };

    public AnimationClip WoodmanWalk;
    
    private void Start()
    {
        // StartCoroutine(WaitForEndOfText(2));
    }

    public IEnumerator WaitForEndOfText(int levelNumber)
    {
        var superTextMesh = FindObjectOfType<SuperTextMesh>();

        if (levelNumber == 4 || levelNumber == 5)
        {
            var knightSprite = GameObject.Find("final-knight").GetComponent<SpriteAnim>();
            knightSprite.Play(WoodmanWalk);
        }


        superTextMesh.Text = TheScript[levelNumber - 1];
        
        while (superTextMesh.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(5f);

        SceneManager.LoadScene($"Area-{(levelNumber+1).ToString("D2")}");

        yield return null;
    }
}
