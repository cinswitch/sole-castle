﻿using UnityEngine;
using System.Collections;

public class GadgetDamage : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var parent = transform.parent;
        if (collision.name.StartsWith("attack"))
        {
            parent.GetComponent<Gadget>().Interupt("attack");
            return;
        }
    }
}
