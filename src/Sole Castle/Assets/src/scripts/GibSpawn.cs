﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GibSpawn : MonoBehaviour
{
    public GameObject BoneGib;

    IEnumerator _gibsRoutine;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (new List<string> { "collider", "floor", "skeleton", "damage-collider" }.Contains(collision.name))
        {
            return;
        }
        
        var parent = transform.parent;
        //Debug.Log(parent.name);

        if (parent.GetComponent<Skeleton>() != null)
        {
            if (collision.name == "feet")
            {
                parent.GetComponent<Skeleton>().Interupt("kill");
                return;
            }

            parent.GetComponent<Skeleton>().Interupt("attack");
        }
        if (parent.GetComponent<SkeletonThrower>() != null)
        {
            if (collision.name == "feet")
            {
                parent.GetComponent<SkeletonThrower>().Interupt("kill");
                return;
            }

            parent.GetComponent<SkeletonThrower>().Interupt("attack");
        }





        //if (_gibsRoutine == null)
        //{
        //   _gibsRoutine = SpawnGibs(transform.position + Vector3.up);
        //
        //   StartCoroutine(_gibsRoutine);
        //}
    }

    public IEnumerator SpawnGibs(Vector3 position)
    {
        var gib1 = Instantiate<GameObject>(BoneGib);
        var gib2 = Instantiate<GameObject>(BoneGib);
        var gib3 = Instantiate<GameObject>(BoneGib);

        gib1.transform.position = transform.position;
        gib2.transform.position = transform.position;
        gib3.transform.position = transform.position;

        gib1.GetComponent<Rigidbody2D>().AddForce(5f * (Vector3.up + Vector3.right), ForceMode2D.Impulse);
        gib2.GetComponent<Rigidbody2D>().AddForce(5f * (Vector3.right), ForceMode2D.Impulse);
        gib3.GetComponent<Rigidbody2D>().AddForce(5f * (Vector3.down + Vector3.right), ForceMode2D.Impulse);

        yield return new WaitForSeconds(1f);

        _gibsRoutine = null;
    }
}
