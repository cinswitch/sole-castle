﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using PowerTools;

public class SkeletonThrower : MonoBehaviour
{
    public GameObject BoneToThrow;

    IEnumerator _mainRoutine;
    IEnumerator _attackRoutine;
    IEnumerator _damageRoutine;

    public AnimationClip Idle;
    public AnimationClip Jump;
    public AnimationClip Land;
    public AnimationClip Hop;

    public AudioClip AttackClip;
    public AudioClip JumpClip;
    public AudioClip DieClip;

    private AudioSource AudioSource;

    private SpriteAnim SpriteAnim;

    public GameObject BoneGib;

    private int health = 1;

    public void Start()
    {
        AudioSource = gameObject.AddComponent<AudioSource>();

        _mainRoutine = MainRoutine();
        StartCoroutine(_mainRoutine);
    }

    public IEnumerator MainRoutine()
    {
        SpriteAnim = GetComponentInChildren<SpriteAnim>();
        
        // Just a hack to make our skelly boys out of sync
        //TODO: Wait until the skeleton is either just off camera, or just on camera before starting this part of the party.
        // yield return new WaitForSeconds(Random.Range(0f, 1f));

        var rigidBody = GetComponent<Rigidbody2D>();

        var player = GameObject.Find("person");
        
        while (true)
        {
            var xPos = transform.position.x;
            var playerXPos = player.transform.position.x;

            if (xPos - playerXPos < 15f)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        while (true)
        {
            var playerXPos = player.transform.position.x;
            var xPos = transform.position.x;

            // Stop doing things if the player is reasonably past the skeleton
            if (playerXPos - xPos > 6.5f)
            {
                break;
            }

            SpriteAnim.Play(Idle);

            yield return new WaitForSeconds(1f);
            var movePick = Random.Range(0, 3);

            if (movePick == 2)
            {
                _attackRoutine = DoTripleAttack(rigidBody);
                yield return StartCoroutine(_attackRoutine);
                continue;
            }

            Debug.Log("Start attack");
            _attackRoutine = DoAttack(rigidBody);
            yield return StartCoroutine(_attackRoutine);
            Debug.Log("End of attack");
        }
    }

    private IEnumerator DoAttack(Rigidbody2D rigidBody)
    {
        transform.Find("sprite").transform.DOShakePosition(.25f / 2f, .25f, 40);

        yield return new WaitForSeconds(.25f);

        // Play some kind of throwing animation, possibly move the bone creation to the "toss" frame of the animation
        AudioSource.PlayOneShot(AttackClip);

        var bone = Instantiate(BoneToThrow, transform.position + (Vector3.up * .5f + Vector3.left * .5f), Quaternion.identity);
        var boneRigidbody = bone.GetComponent<Rigidbody2D>();
        boneRigidbody.velocity = (Vector3.up * 10f + Vector3.left * 3f);
        boneRigidbody.angularVelocity = 360f * 3f;

        yield return new WaitForSeconds(.75f);
        
        yield return new WaitForSeconds(.5f);
        AudioSource.PlayOneShot(JumpClip);
        SpriteAnim.Play(Hop);
        rigidBody.velocity = (Vector3.up * 2f + Vector3.right * 2f);
        
        yield return new WaitForSeconds(.5f);
        AudioSource.PlayOneShot(JumpClip);
        SpriteAnim.Play(Hop);
        rigidBody.velocity = (Vector3.up * 2f + Vector3.left * 2f);

        yield return new WaitForSeconds(.25f);

        yield return null;
    }

    private IEnumerator DoTripleAttack(Rigidbody2D rigidBody)
    {
        transform.Find("sprite").transform.DOShakePosition(.25f / 2f, .25f, 40);
        rigidBody.velocity = (Vector3.up * 5f);

        AudioSource.PlayOneShot(JumpClip);
        SpriteAnim.Play(Jump);
        yield return new WaitForSeconds(.5f);

        // Play some kind of throwing animation, possibly move the bone creation to the "toss" frame of the animation

        AudioSource.PlayOneShot(AttackClip);
        var bone = Instantiate(BoneToThrow, transform.position + (Vector3.up * .5f + Vector3.left * .5f), Quaternion.identity);
        var boneRigidbody = bone.GetComponent<Rigidbody2D>();
        boneRigidbody.velocity = (Vector3.up * 10f + Vector3.left * 3f);
        boneRigidbody.angularVelocity = 360f * 3f;
        yield return new WaitForSeconds(.1f);
        AudioSource.PlayOneShot(AttackClip);
        bone = Instantiate(BoneToThrow, transform.position + (Vector3.up * .5f + Vector3.left * .5f), Quaternion.identity);
        boneRigidbody = bone.GetComponent<Rigidbody2D>();
        boneRigidbody.velocity = (Vector3.up * 10f + Vector3.left * 3f);
        boneRigidbody.angularVelocity = 360f * 3f;
        yield return new WaitForSeconds(.1f);
        AudioSource.PlayOneShot(AttackClip);
        bone = Instantiate(BoneToThrow, transform.position + (Vector3.up * .5f + Vector3.left * .5f), Quaternion.identity);
        boneRigidbody = bone.GetComponent<Rigidbody2D>();
        boneRigidbody.velocity = (Vector3.up * 10f + Vector3.left * 3f);
        boneRigidbody.angularVelocity = 360f * 3f;
        yield return new WaitForSeconds(.1f);

        SpriteAnim.Play(Land);
        yield return new WaitForSeconds(.75f);

        yield return null;
    }


        //TODO: Duplicated from Skeleton
    public void Interupt(string type)
    {
        if (_attackRoutine != null)
        {
            StopCoroutine(_attackRoutine);
            _attackRoutine = null;
        }

        if (_mainRoutine != null)
        {
            StopCoroutine(_mainRoutine);
            _mainRoutine = null;
        }

        if (type == "attack" || type == "kill")
        {
            if (_damageRoutine == null)
            {
                _damageRoutine = DoDamage(GetComponent<Rigidbody2D>());
                StartCoroutine(_damageRoutine);
            }
        }
    }
    //TODO: Duplicated from Skeleton
    private IEnumerator DoDamage(Rigidbody2D rigidBody)
    {
        health -= 1;

        if (health <= 0)
        {
            SpawnSomeGibsAndKillSelf();

            yield break;
        }

        rigidBody.velocity = (Vector3.up * .5f + Vector3.right * .5f);
        rigidBody.position = new Vector2(rigidBody.position.x + .25f, rigidBody.position.y + .25f);
        rigidBody.freezeRotation = false;
        rigidBody.angularVelocity = -60f;

        yield return new WaitForSeconds(.1f);

        rigidBody.freezeRotation = true;
        rigidBody.angularVelocity = 0f;
        rigidBody.rotation = 0f;
        rigidBody.velocity = Vector3.zero;

        yield return new WaitForSeconds(.05f);

        _mainRoutine = MainRoutine();
        StartCoroutine(_mainRoutine);

        _damageRoutine = null;
    }

    //TODO: Duplicated from Skeleton
    private void SpawnSomeGibsAndKillSelf()
    {
        var soundObject = new GameObject();
        var source = soundObject.AddComponent<AudioSource>();
        source.PlayOneShot(DieClip);

        var relativePositions = new List<Vector2>
            {
                Vector3.zero,
                Vector3.up,
                Vector3.right * .3f,
                Vector3.left * .3f,
                Vector3.up * 1.2f + Vector3.right * .3f,
                Vector3.up * 1.2f + Vector3.left * .3f,
                Vector3.up * 1.3f + Vector3.right * .3f,
                Vector3.up * 1.3f + Vector3.left * .3f,
                Vector3.up * 1.4f + Vector3.right * .3f,
                Vector3.up * 1.4f + Vector3.left * .3f
            };

        foreach (var pos in relativePositions)
        {
            var gib = Instantiate<GameObject>(BoneGib);
            gib.transform.position = transform.position + new Vector3(pos.x, pos.y, 0f);
            gib.GetComponent<Rigidbody2D>().AddForce(2f * (Vector3.up + Vector3.right), ForceMode2D.Impulse);
        }

        GetComponentsInChildren<SpriteRenderer>().ToList().ForEach(c => c.enabled = false);

        Destroy(gameObject);
    }
}