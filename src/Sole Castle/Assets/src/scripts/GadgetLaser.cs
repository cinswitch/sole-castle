﻿using UnityEngine;
using System.Collections;

public class GadgetLaser : MonoBehaviour
{
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(-5f, -2.5f) * 5f;

        Destroy(gameObject, 5f);
    }
}
