﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerGui : MonoBehaviour
{
    public GameObject HealthIndicator;
    public GameObject StaminaIndicator;

    private List<GameObject> Healths = new List<GameObject>();
    private List<GameObject> Staminas = new List<GameObject>();

    //void Start ()
    //{
    //    var healthInitialPosiiton = GameObject.Find("health-indicator-initial-pos");
    //    var staminaInitialPosiiton = GameObject.Find("stamina-indicator-initial-pos");

    //    var numberOfHealths = 3;

    //    for (int i = numberOfHealths; i >= 0; i--)
    //    {
    //        var health = Instantiate(HealthIndicator);
    //        health.transform.position = healthInitialPosiiton.transform.position + new Vector3((i - 1) * 1f, 0, 0);
    //        Healths.Add(health);
    //    }

    //    for (int i = 3; i >= 0; i--)
    //    {
    //        var stamina = Instantiate(StaminaIndicator);
    //        stamina.transform.position = staminaInitialPosiiton.transform.position + new Vector3((i - 1) * 2f, 0, 0);
    //        Staminas.Add(stamina);
    //    }

    //    Destroy(healthInitialPosiiton);
    //    Destroy(staminaInitialPosiiton);
    //}

    public void Init(int maxHealth, int maxStamina)
    {
        var healthInitialPosiiton = GameObject.Find("health-indicator-initial-pos");
        var staminaInitialPosiiton = GameObject.Find("stamina-indicator-initial-pos");

        var numberOfHealths = 3;

        for (int i = numberOfHealths; i > 0; i--)
        {
            var health = Instantiate(HealthIndicator);
            health.name = HealthIndicator.name;
            health.transform.position = healthInitialPosiiton.transform.position + new Vector3((i - 1) * 1f, 0, 0);
            health.transform.parent = gameObject.transform;
            Healths.Add(health);
        }

        Healths.Reverse();


        // On the final level we have a static gui element instead
        if (maxStamina < 50)
        {
            for (int i = maxStamina; i > 0; i--)
            {
                var stamina = Instantiate(StaminaIndicator);
                stamina.name = StaminaIndicator.name;
                stamina.transform.position = staminaInitialPosiiton.transform.position + new Vector3((i - 1) * 1f, 0, 0);
                stamina.transform.parent = gameObject.transform;
                Staminas.Add(stamina);


            }

            Staminas.Reverse();

            Destroy(staminaInitialPosiiton);
        }
        
        Destroy(healthInitialPosiiton);
    }

    public void UpdateHealth(int num)
    {
        var shownHealths = Healths.Take(num).ToList();
        shownHealths.ForEach(s => s.SetActive(true));

        if (Healths.Count <= num)
        {
            return;
        }

        var hiddenHealths = Healths.Skip(num).ToList();
        hiddenHealths.ForEach(s => s.SetActive(false));
    }

    public void UpdateStamina(int num)
    {
        var shownStaminas = Staminas.Take(num).ToList();
        shownStaminas.ForEach(s => s.SetActive(true));

        if (Staminas.Count <= num)
        {
            return;
        }

        var hiddenStaminas = Staminas.Skip(num).ToList();
        hiddenStaminas.ForEach(s => s.SetActive(false));
    }
}
