﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraFollow : MonoBehaviour
{
    // How quickly the camera accelerates to keep up with the player.
    // Keep it under 1f.
    public float cameraSnapX = .2f;
    public float maxSpeedX = 80f;

    // Distance the player can move away from the camera center before it starts trying to recenter.
    public float cameraFreeX = 1f;

    public GameObject Player;
    public GameObject cameraCenter;

    public bool Pause;

    private Vector3 _initialPosition;

    public void Start()
    {
        _initialPosition = transform.localPosition;

        //Pause = false;
        Player = FindObjectOfType<Move>()?.gameObject;
    }

    private void FixedUpdate()
    {
        if (Player == null)
        {
            Player = FindObjectOfType<Move>()?.gameObject;
            if (Player == null)
            {
                return;
            }
        }

        if (Pause)
        {
            return;
        }

        MoveCamera(cameraCenter.transform, Player.transform);
    }

    public void MoveCamera(Transform cameraTransform, Transform targetTransform)
    {
        // TODO: Maybe let's use DoTween and have it be reasonably smooth instead of falling on our old iTween business
        var positionDifference = targetTransform.position - cameraTransform.position;
        float xSpeed = Mathf.Abs(positionDifference.x) * cameraSnapX;

        //Cap the camera's speed so it doesn't go fucking nuts and start overshooting the player
        if (xSpeed > maxSpeedX) { xSpeed = maxSpeedX; }

        if (positionDifference.x >= cameraFreeX)
        {
            cameraTransform.position = cameraTransform.position.SetX(FloatUpdate(cameraTransform.position.x, targetTransform.position.x, xSpeed));
        }
    }

    private float FloatUpdate(float currentValue, float targetValue, float speed)
    {
        float diff = targetValue - currentValue;
        currentValue += (diff * speed) * Time.deltaTime;
        return (currentValue);
    }

    public void ResetPosition()
    {
        transform.localPosition = _initialPosition;
    }

    public void SetCenterPosition(Vector3 position)
    {
        cameraCenter.transform.position = position;
    }
}
