﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndingChoice : MonoBehaviour
{

    IEnumerator Main()
    {
        var choices = GameObject.Find("choices");
        choices.SetActive(false);

        var textMesh = GameObject.Find("TheEndingText").GetComponent<SuperTextMesh>();

        while (textMesh.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(3f);

        GameObject.Find("TheEndingText").SetActive(false);
        choices.SetActive(true);

        GameObject.Find("EndingAChoice").GetComponent<SuperTextButton>().SetClickAction(() => { SceneManager.LoadScene("TheEnd-A"); });
        GameObject.Find("EndingBChoice").GetComponent<SuperTextButton>().SetClickAction(() => { SceneManager.LoadScene("TheEnd-B"); });

    }
}
