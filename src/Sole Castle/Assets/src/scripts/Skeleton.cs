﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using PowerTools;

public class Skeleton : MonoBehaviour
{
    IEnumerator _mainRoutine;
    IEnumerator _attackRoutine;
    IEnumerator _damageRoutine;

    public AnimationClip SkeletonIdle;
    public AnimationClip SkeletonJump;
    public AnimationClip SkeletonRun;
    public AnimationClip SkeletonStab;

    public AudioClip BootlegDeadClip;
    public AudioClip BootlegSwingClip;
    public AudioClip BootlegRunClip;

    public AudioClip DeadClip;
    public AudioClip HurtClip;
    public AudioClip SwingClip;
    public AudioClip JumpClip;

    private AudioSource AudioSource;
    private AudioSource RunAudioSource;

    private SpriteAnim SpriteAnim;

    public GameObject BoneGib;

    private BoxCollider2D _attackCollider;

    private int health = 1;

    bool IsBootleg = false;

    public void Start()
    {
        AudioSource = gameObject.AddComponent<AudioSource>();
        RunAudioSource = gameObject.AddComponent<AudioSource>();
        RunAudioSource.loop = true;
        RunAudioSource.clip = BootlegRunClip;

        IsBootleg = FindObjectOfType<GameManager>().CartoonySpinEnabled();
        
        _mainRoutine = MainRoutine();
        StartCoroutine(_mainRoutine);
    }

    public IEnumerator MainRoutine()
	{
        SpriteAnim = GetComponentInChildren<SpriteAnim>();

        var attackCollider = transform.Find("attack-collider");
        _attackCollider = attackCollider.GetComponent<BoxCollider2D>();
        _attackCollider.enabled = true;
        _attackCollider.gameObject.SetActive(false);

        // Just a hack to make our skelly boys out of sync
        //TODO: Wait until the skeleton is either just off camera, or just on camera before starting this part of the party.
        //yield return new WaitForSeconds(Random.Range(0f, 1f));

        var rigidBody = GetComponent<Rigidbody2D>();

        // Wait for the player to get within a proper distance before starting up

        var player = GameObject.Find("person");

        SpriteAnim.Play(SkeletonIdle);

        while (true)
        {
            var xPos = transform.position.x;
            var playerXPos = player.transform.position.x;

            if (xPos - playerXPos < 8f)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }
        
		while (true)
        {
            var xPos = transform.position.x;
            var playerXPos = player.transform.position.x;

            // Stop doing things if the player is reasonably past the skeleton
            if (playerXPos - xPos > 6.5f)
            {
                break;
            }

            if (xPos - playerXPos > 6f)
            {
                if (!SpriteAnim.IsPlaying(SkeletonRun))
                {
                    SpriteAnim.Play(SkeletonRun);
                }
                
                rigidBody.velocity = SetX(rigidBody, -1 * 3f);

                if (IsBootleg && !RunAudioSource.isPlaying)
                {
                    RunAudioSource.Play();
                }

                yield return new WaitForEndOfFrame();

                continue;
            }
            else if (RunAudioSource.isPlaying)
            {
                RunAudioSource.Stop();
            }


            //SpriteAnim.Play(SkeletonIdle);

            yield return new WaitForSeconds(.5f);

            var movePick = Random.Range(0, 4);
            if (movePick == 3)
            {
                AudioSource.PlayOneShot(JumpClip);
                SpriteAnim.Play(SkeletonJump);
                rigidBody.velocity = (Vector3.up * 2f + Vector3.right * 4f);

                yield return new WaitForSeconds(.25f);
            }
            if (movePick == 2)
            {
                _attackRoutine = DoQuickAttack(rigidBody);
                yield return StartCoroutine(_attackRoutine);
            }
            if (movePick <= 1)
            {
                _attackRoutine = DoAttack(rigidBody);
                yield return StartCoroutine(_attackRoutine);
            }
        }
	}

    public void Interupt(string type)
    {
        if (_attackRoutine != null)
        {
            StopCoroutine(_attackRoutine);
            _attackRoutine = null;
        }

        if (_mainRoutine != null)
        {
            StopCoroutine(_mainRoutine);
            _mainRoutine = null;
        }

        if (type == "attack")
        {
            if (_damageRoutine == null)
            {
                _damageRoutine = DoDamage(GetComponent<Rigidbody2D>());
                StartCoroutine(_damageRoutine);
            }
        }
        if (type == "kill")
        {
            health = 0;

            if (_damageRoutine == null)
            {
                _damageRoutine = DoDamage(GetComponent<Rigidbody2D>());
                StartCoroutine(_damageRoutine);
            }
        }
    }

    private IEnumerator DoDamage(Rigidbody2D rigidBody)
    {
        health -= 1;

        if (FindObjectOfType<GameManager>().CartoonySpinEnabled())
        {
            yield return DoDamageWithCartoonyRotation(rigidBody);
            yield break;
        }


        if (health <= 0)
        {
            SpawnSomeGibsAndKillSelf();

            yield break;
        }

        rigidBody.velocity = (Vector3.up * .5f + Vector3.right * .5f);
        rigidBody.position = new Vector2(rigidBody.position.x + .25f, rigidBody.position.y + .25f);
        rigidBody.freezeRotation = false;
        rigidBody.angularVelocity = -60f;

        yield return new WaitForSeconds(.1f);

        rigidBody.freezeRotation = true;
        rigidBody.angularVelocity = 0f;
        rigidBody.rotation = 0f;
        rigidBody.velocity = Vector3.zero;

        yield return new WaitForSeconds(.05f);

        _mainRoutine = MainRoutine();
        StartCoroutine(_mainRoutine);

        _damageRoutine = null;
    }

    private void SpawnSomeGibsAndKillSelf()
    {
        var soundObject = new GameObject();
        var source = soundObject.AddComponent<AudioSource>();
        source.PlayOneShot(DeadClip);

        Destroy(soundObject, 3f);

        var relativePositions = new List<Vector2>
            {
                Vector3.zero,
                Vector3.up,
                Vector3.right * .3f,
                Vector3.left * .3f,
                Vector3.up * 1.2f + Vector3.right * .3f,
                Vector3.up * 1.2f + Vector3.left * .3f,
                Vector3.up * 1.3f + Vector3.right * .3f,
                Vector3.up * 1.3f + Vector3.left * .3f,
                Vector3.up * 1.4f + Vector3.right * .3f,
                Vector3.up * 1.4f + Vector3.left * .3f
            };

        foreach (var pos in relativePositions)
        {
            var gib = Instantiate<GameObject>(BoneGib);
            gib.transform.position = transform.position + new Vector3(pos.x, pos.y, 0f);
            gib.GetComponent<Rigidbody2D>().AddForce(2f * (Vector3.up + Vector3.right), ForceMode2D.Impulse);
        }

        GetComponentsInChildren<SpriteRenderer>().ToList().ForEach(c => c.enabled = false);

        Destroy(gameObject);
    }
    
    private IEnumerator DoDamageWithCartoonyRotation(Rigidbody2D rigidBody)
    {
        _attackCollider.gameObject.SetActive(false);

        AudioSource.PlayOneShot(BootlegDeadClip);

        rigidBody.velocity = (Vector3.up * 6f + Vector3.right * 4f);
        rigidBody.position = new Vector2(rigidBody.position.x + .25f, rigidBody.position.y + .25f); 
        rigidBody.freezeRotation = false;
        rigidBody.angularVelocity = 2400f;
        transform.Find("collider").gameObject.layer = LayerMask.NameToLayer("PlayerAttacks");

        yield return new WaitForSeconds(10f);

        Destroy(gameObject);
    }

    private IEnumerator DoQuickAttack(Rigidbody2D rigidBody)
    {
        rigidBody.velocity = (Vector3.up * 5f);

        AudioSource.PlayOneShot(JumpClip);
        SpriteAnim.Play(SkeletonJump);
        rigidBody.velocity = (Vector3.up * 1f + Vector3.right * 2f);

        yield return new WaitForSeconds(.5f);

        AudioSource.PlayOneShot(JumpClip);
        SpriteAnim.Play(SkeletonJump);
        rigidBody.velocity = (Vector3.up * 1f + Vector3.left * 2f);

        yield return new WaitForSeconds(.5f);

        AudioSource.PlayOneShot(JumpClip);
        SpriteAnim.Play(SkeletonJump);
        rigidBody.velocity = (Vector3.up * 1f + Vector3.left * 2f);

        yield return new WaitForSeconds(.5f);

        rigidBody.velocity = SetX(rigidBody, -1 * 8f);
        SpriteAnim.Play(SkeletonStab);

        if (IsBootleg)
        {
            AudioSource.PlayOneShot(BootlegSwingClip);
        }
        else
        {
            AudioSource.PlayOneShot(SwingClip);
        }

        yield return new WaitForSeconds(.25f);
        
        _attackCollider.gameObject.SetActive(true);
        yield return new WaitForSeconds(.25f);
        
        SpriteAnim.Play(SkeletonJump);
        _attackCollider.gameObject.SetActive(false);
        rigidBody.velocity = (Vector3.up * 2f + Vector3.right * 4f);

        yield return new WaitForSeconds(.5f);
        
        SpriteAnim.Play(SkeletonRun);
        rigidBody.velocity = (Vector3.right) * 2f;

        yield return new WaitForSeconds(.25f);

        SpriteAnim.Play(SkeletonIdle);
        rigidBody.velocity = SetX(rigidBody, 0f);

        yield return new WaitForEndOfFrame();
    }

    private IEnumerator DoAttack(Rigidbody2D rigidBody)
    {
        transform.Find("sprite").transform.DOShakePosition(.25f / 2f, .25f, 40);
        
        yield return new WaitForSeconds(.25f);

        SpriteAnim.Play(SkeletonStab);
        rigidBody.velocity = SetX(rigidBody, -1 * 4f);

        if (IsBootleg)
        {
            AudioSource.PlayOneShot(BootlegSwingClip);
        }
        else
        {
            AudioSource.PlayOneShot(SwingClip);
        }

        yield return new WaitForSeconds(.25f);
        
        _attackCollider.gameObject.SetActive(true);
        yield return new WaitForSeconds(.75f);
        
        SpriteAnim.Play(SkeletonIdle);
        rigidBody.velocity = SetX(rigidBody, 0f);

        //_attackCollider.enabled = false;
        _attackCollider.gameObject.SetActive(false);
    }

    private Vector2 SetX(Rigidbody2D rigidBody, float x)
    {
        return new Vector2(x, rigidBody.velocity.y);
    }
}
