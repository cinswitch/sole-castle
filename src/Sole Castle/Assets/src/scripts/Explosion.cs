﻿using UnityEngine;
using System.Collections;
using PowerTools;

public class Explosion : MonoBehaviour
{

    public AudioClip Explod1;
    public AudioClip Explod2;

    AudioSource AudioSource;

    void Start()
    {
        AudioSource = gameObject.AddComponent<AudioSource>();
        if (Random.Range(0, 2) == 0)
        {
            transform.localScale = transform.localScale * 1.5f;
            AudioSource.PlayOneShot(Explod1, .5f);
        }
        else
        {
            AudioSource.PlayOneShot(Explod2, .5f);
        }
    }
}
